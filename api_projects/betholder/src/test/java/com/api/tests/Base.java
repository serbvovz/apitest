package com.api.tests;

import io.restassured.RestAssured;
import org.junit.jupiter.api.BeforeAll;

public class Base {

    @BeforeAll
    static void setUp() {
        RestAssured.baseURI = "http://api.bexxxxxxxx.xx/xx";
    }
}
