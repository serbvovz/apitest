package com.api.tests;

import com.api.entity.User;
import com.api.entity.Wallet;
import com.api.response.ApiResponse;
import com.api.service.UserApiServiceAdvanced;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.Test;

import static com.api.conditions.Conditions.bodyField;
import static com.api.conditions.Conditions.statusCode;
import static org.hamcrest.Matchers.*;

public class UserApiTests extends Base {

    private UserApiServiceAdvanced userApiService = new UserApiServiceAdvanced();

    @Test
    void testCanRegisterAsValidUser() {
        // Given
        User user = new User()
                .setDeviceIdentifier("test")
                .setDevicePlatform("ios")
                .setUserType("wallet_tracker")
                .setEmail(RandomStringUtils.randomAlphabetic(5) + "superusermobileqatest@qa.com")
                .setPassword("test")
                .setName("test");
        // When
        ApiResponse response = userApiService.registerUser(user);
        //Then
        response.shouldHave(statusCode(201));
        response.shouldHave(bodyField("api_key", not(isEmptyOrNullString())));
    }

    @Test
    void testCanNotRegisterSameUsernameTwice() {
        // Given
        User user = new User()
                .setDeviceIdentifier("test")
                .setDevicePlatform("ios")
                .setUserType("wallet_tracker")
                .setEmail("superusermobileqatest@qa.com")
                .setPassword("test")
                .setName("test");
        // When
        ApiResponse response = userApiService.registerUser(user);
        //Then
        response.shouldHave(statusCode(422));
    }

    @Test
    void testCanUserLogin() {
        // Given
        User user = new User()
                .setDeviceIdentifier("test")
                .setDevicePlatform("ios")
                .setUserType("wallet_tracker_bl")
                .setEmail("serbvovz@gmail.com")
                .setPassword("qwertyui")
                .setName("test");
        // When
        ApiResponse response = userApiService.loginUser(user);
        //Then
        response.shouldHave(statusCode(201));
        response.shouldHave(bodyField("api_key", not(isEmptyOrNullString())));
    }

    @Test
    void testCanUserCreateWallet() {
        User user = new User()
                .setDeviceIdentifier("test")
                .setDevicePlatform("ios")
                .setUserType("wallet_tracker")
                .setEmail(RandomStringUtils.randomAlphabetic(5) + "superusermobileqatest@qa.com")
                .setPassword("test")
                .setName("test");
        ApiResponse response = userApiService.registerUser(user);
        String apiKeyVariable = response.getBodyField("api_key");

        Wallet wallet = new Wallet()
                .setWalletType("eth")
                .setAddress("0x9b538c3d144dc8b8648280E8fC0b8aF334e7b312");

        ApiResponse response1 = userApiService.createWallet(wallet, apiKeyVariable);

        response1.shouldHave(statusCode(201));
        response1.shouldHave(bodyField("id", not(isEmptyOrNullString())));
    }

    @Test
    void testPasswordNotValid() {
        // Given
        User user = new User()
                .setDeviceIdentifier("test")
                .setDevicePlatform("ios")
                .setUserType("wallet_tracker")
                .setEmail("superusermobileqatest@qa.com")
                .setPassword("123")
                .setName("test");
        // When
        ApiResponse response = userApiService.loginUser(user);
        //Then
        response.shouldHave(statusCode(422));
        response.shouldHave(bodyField("code", is("auth_error")));
    }

    @Test
    void testLoginNotValid() {
        // Given
        User user = new User()
                .setDeviceIdentifier("test")
                .setDevicePlatform("ios")
                .setUserType("wallet_tracker")
                .setEmail("123")
                .setPassword("test")
                .setName("test");
        // When
        ApiResponse response = userApiService.loginUser(user);
        //Then
        response.shouldHave(statusCode(422));
        response.shouldHave(bodyField("code", is("auth_error")));
    }
}
