package com.api.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

import javax.annotation.Generated;

@Getter
@Setter
@ToString
@NoArgsConstructor
@Accessors(chain = true)
@Generated("com.robohorse.robopojogenerator")
public class Transaction {

    @JsonProperty("wallet_id")
    private String walletId;
}