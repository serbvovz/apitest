package com.api.service;

import com.api.entity.User;
import com.api.entity.Wallet;
import com.api.response.ApiResponse;
import io.qameta.allure.Step;
import io.qameta.allure.restassured.AllureRestAssured;
import io.restassured.RestAssured;
import io.restassured.filter.Filter;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;

@Slf4j
public class UserApiServiceAdvanced {

    private RequestSpecification given() {
        List<Filter> filters = new ArrayList<>();
        if (Boolean.valueOf(System.getProperty("enableLogging", "false"))) {

            filters.add(new RequestLoggingFilter());
            filters.add(new ResponseLoggingFilter());
        }

        filters.add(new AllureRestAssured());

        return RestAssured
                .given()
                .contentType(ContentType.JSON)
                .filters(filters);
    }

    @Step
    public ApiResponse registerUser(User user) {
        log.info("register user {}", user);

        Response response = given()
                .body(user)
                .when()
                .post("/users/register")
                .then().extract().response();
        return new ApiResponse(response);
    }

    @Step
    public ApiResponse loginUser(User user) {
        log.info("login user {}", user);

        Response response = given()
                .body(user)
                .when()
                .post("/users/login")
                .then().extract().response();
        return new ApiResponse(response);
    }

    @Step
    public ApiResponse createWallet(Wallet wallet, String userKey) {
        log.info("user {}", wallet);

        Response response = given()
                .body(wallet).header("Authorization", userKey)
                .when()
                .post("/wallets")
                .then().extract().response();
        return new ApiResponse(response);
    }
}
