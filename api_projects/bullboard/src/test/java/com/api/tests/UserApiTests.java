package com.api.tests;

import com.api.entity.User;
import com.api.entity.Wallet;
import com.api.response.ApiResponse;
import com.api.service.UserApiServiceAdvanced;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.Test;

import static com.api.conditions.Conditions.bodyField;
import static com.api.conditions.Conditions.statusCode;
import static org.hamcrest.Matchers.isEmptyOrNullString;
import static org.hamcrest.Matchers.not;

public class UserApiTests extends Base {

    private UserApiServiceAdvanced userApiService = new UserApiServiceAdvanced();

    @Test
    void testCanRegisterAsValidUser() {
        // Given
        User user = new User()
                .setDeviceIdentifier("test")
                .setDevicePlatform("ios")
                .setEmail(RandomStringUtils.randomAlphabetic(5) + "superusermobileqatest@qa.com")
                .setPassword("test")
                .setName("test");
        // When
        ApiResponse response = userApiService.registerUser(user);
        //Then
        response.shouldHave(statusCode(201));
        response.shouldHave(bodyField("api_key", not(isEmptyOrNullString())));
    }

    @Test
    void testCanNotRegisterSameUsernameTwice() {
        // Given
        User user = new User()
                .setDeviceIdentifier("test")
                .setDevicePlatform("ios")
                .setEmail("superusermobileqatest@qa.com")
                .setPassword("test")
                .setName("test");
        // When
        ApiResponse response = userApiService.registerUser(user);
        //Then
        response.shouldHave(statusCode(422));
    }

    @Test
    void testCanUserLogin() {
        // Given
        User user = new User()
                .setDeviceIdentifier("test")
                .setDevicePlatform("ios")
                .setEmail("superusermobileqatest@qa.com")
                .setPassword("test")
                .setName("test");
        // When
        ApiResponse response = userApiService.loginUser(user);
        //Then
        response.shouldHave(statusCode(201));
        response.shouldHave(bodyField("api_key", not(isEmptyOrNullString())));
    }

    @Test
    void testCanLogout() {
        User user = new User()
                .setDeviceIdentifier("test")
                .setDevicePlatform("ios")
                .setEmail(RandomStringUtils.randomAlphabetic(5) + "superusermobileqatest@qa.com")
                .setPassword("test")
                .setName("test");
        ApiResponse response = userApiService.registerUser(user);
        String apiKeyVariable = response.getBodyField("api_key");

        Wallet wallet = new Wallet()
                .setAddress("0x9b538c3d144dc8b8648280E8fC0b8aF334e7b312");

        ApiResponse response1 = userApiService.logOut(wallet, apiKeyVariable);

        response1.shouldHave(statusCode(200));
    }
}
